# 💉 Syringe

Syringe is a straightforward dependency injection framework for Go.

## Usage

Syringe is based around containers. A container allows registering bindings.

```go
import (
  "fmt"

  "gitlab.com/codingpaws/syringe"
)

type Dog struct {
  Name string
}

func main() {
  container := syringe.New()

  // Register a binding
  container.Bind(func() Dog {
    return Pet{
      Name: "Luna"
    }
  })

  // Get the model from
  var dog Dog
  container.Get(&dog)
  fmt.Println(dog.Name) // Prints "Luna"

  // Call a function with auto-filled arguments
  container.Call(func (d Dog) {
    fmt.Println(d.Name) // Prints "Luna"
  })
}
```
