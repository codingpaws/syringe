package syringe

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/codingpaws/syringe/bindings"
)

type invoker func(Container) interface{}

type Container interface {
	Bind(fn interface{}) error
	Singleton(fn interface{}) error
	Call(fn interface{}) (interface{}, error)
	Make(typ interface{}) interface{}
	SafeMake(typ interface{}) (interface{}, error)
	Get(typ interface{}) error
	Clone() Container
}

type container struct {
	parent   *container
	bindings map[bindings.Key]invoker
}

func New() Container {
	return container{
		bindings: make(map[bindings.Key]invoker),
	}
}

func (c container) Clone() Container {
	cloned := New().(container)
	cloned.parent = &c

	return cloned
}

func (c container) Get(typ interface{}) (err error) {
	t := reflect.TypeOf(typ)
	invoke, err := c.getBinding(t.Elem())

	if err != nil {
		return
	}

	value := reflect.ValueOf(typ)
	value.Elem().Set(reflect.ValueOf(invoke(c)))

	return nil
}

func (c container) Bind(what interface{}) (err error) {
	var typ reflect.Type

	if typ, err = getFirstReturnType(what); err != nil {
		return
	}

	return c.bindInvoker(typ, func(c Container) interface{} {
		out, _ := c.Call(what)

		return out
	})
}

func (c container) bindInvoker(typ interface{}, invoke invoker) error {
	key, err := bindings.New(typ)

	if err != nil {
		return err
	}

	c.bindings[key] = invoke

	return nil
}

func (c container) Singleton(what interface{}) (err error) {
	var typ reflect.Type

	if typ, err = getFirstReturnType(what); err != nil {
		return
	}

	var result interface{}

	if result, err = c.Call(what); err != nil {
		return
	}

	return c.bindInvoker(typ, func(c Container) interface{} {
		return result
	})
}

func (c container) Make(typ interface{}) interface{} {
	result, err := c.SafeMake(typ)

	if err != nil {
		panic(err)
	}

	return result
}

func (c container) SafeMake(typ interface{}) (interface{}, error) {
	binding, err := c.getBinding(typ)

	if err != nil {
		return nil, err
	}

	return binding(c), nil
}

func (c container) Call(f interface{}) (interface{}, error) {
	fun := reflect.ValueOf(f)

	if fun.Kind() != reflect.Func {
		return nil, fmt.Errorf("expected %#v to be a func", f)
	}

	args := []reflect.Value{}

	for i := 0; i < fun.Type().NumIn(); i++ {
		typ := fun.Type().In(i)

		x, err := c.SafeMake(typ)

		if err != nil {
			return nil, err
		}

		args = append(args, reflect.ValueOf(x))
	}

	result := fun.Call(args)

	if len(result) == 0 {
		return nil, nil
	}

	if len(result) == 2 && result[1].CanConvert(reflect.TypeOf(errors.New(""))) {
		return result[0].Interface(), result[1].Interface().(error)
	}

	return result[0].Interface(), nil
}

func (c container) getBinding(typ interface{}) (invoker, error) {
	if typ == nil {
		return nil, errors.New("container.getBinding: typ is nil")
	}

	v, err := bindings.New(typ)

	if err != nil {
		return nil, err
	}

	for key, binding := range c.bindings {
		if key == v {
			return binding, nil
		}
	}

	if c.parent != nil {
		return c.parent.getBinding(typ)
	}

	return nil, fmt.Errorf("container.getBinding: no binding for %s", typ)
}
