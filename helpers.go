package syringe

import (
	"errors"
	"reflect"
)

func getFirstReturnType(fn interface{}) (reflect.Type, error) {
	t := reflect.TypeOf(fn)

	if t.NumOut() == 0 {
		return nil, errors.New("func must have at least one return argument")
	}

	return t.Out(0), nil
}
