package bindings_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/codingpaws/syringe/bindings"
)

type ExampleStruct struct {
	Number int
}

func TestInvalidBindingTypes(t *testing.T) {
	testCases := map[string]interface{}{
		"string":     "",
		"number":     10,
		"nil":        nil,
		"struct ref": &ExampleStruct{},
		"slice":      []interface{}{},
		"map":        map[string]bool{},
		"channel":    new(chan int),
	}

	for typeName, tC := range testCases {
		t.Run(typeName, func(t *testing.T) {
			_, err := bindings.New(tC)
			assert.Error(t, err)
		})
	}
}

func TestNew(t *testing.T) {
	typ := ExampleStruct{}
	binding, err := bindings.New(typ)
	assert.Nil(t, err)
	binding2, err := bindings.New(struct{}{})
	assert.Nil(t, err)

	assert.Equal(t, reflect.TypeOf(typ), binding)
	assert.NotEqual(t, binding, binding2)
}
