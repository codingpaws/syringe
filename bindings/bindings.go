package bindings

import (
	"errors"
	"fmt"
	"reflect"
)

type Key reflect.Type

var reflectType = reflect.TypeOf(reflect.Type.Align)

func New(binding interface{}) (key Key, err error) {
	if binding == nil {
		return key, errors.New("binding.From: expected a struct but got nil")
	}

	typ := reflect.TypeOf(binding)

	if typ.Kind() == reflect.Struct {
		key = Key(typ)
	} else if typ == reflect.TypeOf(reflectType) {
		key = Key(binding.(reflect.Type))
	} else {
		err = fmt.Errorf("binding.From: expected a struct but got %s", typ.Name())
	}

	return
}
