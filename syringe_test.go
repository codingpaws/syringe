package syringe_test

import (
	"math/rand"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/codingpaws/syringe"
)

type TestStruct struct {
	Number int
}

type SecondTestStruct struct {
}

func (SecondTestStruct) Name() string {
	return "floof"
}

type ImplementsNameable struct {
}

func (ImplementsNameable) Name() string {
	return "ImplementsNameable"
}

type Nameable interface {
	Name() string
}

func TestNewContainer(t *testing.T) {
	container := syringe.New()

	assert.NotNil(t, container)
}

func TestBind(t *testing.T) {
	container := syringe.New()

	assert.Nil(t, container.Bind(func() TestStruct {
		return TestStruct{Number: rand.Intn(100)}
	}))

	a := container.Make(TestStruct{}).(TestStruct)
	b := container.Make(TestStruct{}).(TestStruct)

	assert.Equal(t, reflect.TypeOf(TestStruct{}), reflect.TypeOf(a))
	assert.Equal(t, reflect.TypeOf(TestStruct{}), reflect.TypeOf(b))

	assert.NotEqual(t, a.Number, b.Number)
}

func TestSingleton(t *testing.T) {
	container := syringe.New()

	container.Singleton(func() TestStruct {
		return TestStruct{Number: rand.Intn(100)}
	})

	a := container.Make(TestStruct{}).(TestStruct)
	b := container.Make(TestStruct{}).(TestStruct)

	assert.Equal(t, reflect.TypeOf(TestStruct{}), reflect.TypeOf(a))
	assert.Equal(t, reflect.TypeOf(TestStruct{}), reflect.TypeOf(b))

	assert.Equal(t, a.Number, b.Number)
}

func TestCall(t *testing.T) {
	container := syringe.New()

	container.Bind(func() TestStruct {
		return TestStruct{Number: rand.Intn(100)}
	})

	container.Bind(func() Nameable {
		return SecondTestStruct{}
	})

	result, err := container.Call(func(a TestStruct, b TestStruct, c Nameable) {
		assert.NotEqual(t, a.Number, b.Number)
		assert.Equal(t, "floof", c.Name())
	})

	assert.Nil(t, result)
	assert.Nil(t, err)
}

func TestOverloading(t *testing.T) {
	container := syringe.New()

	container.Bind(func() Nameable {
		return SecondTestStruct{}
	})

	container.Bind(func() ImplementsNameable {
		return ImplementsNameable{}
	})

	result, err := container.Call(func(abstract Nameable, concrete ImplementsNameable) {
		assert.Equal(t, "floof", abstract.Name())
		assert.Equal(t, "ImplementsNameable", concrete.Name())
	})

	assert.Nil(t, result)
	assert.Nil(t, err)
}

func TestGet(t *testing.T) {
	container := syringe.New()

	assert.Nil(t, container.Bind(func() Nameable {
		return SecondTestStruct{}
	}))

	var nameable Nameable

	assert.Nil(t, container.Get(&nameable))
	assert.Equal(t, "floof", nameable.Name())
}

func TestClone(t *testing.T) {
	container := syringe.New()
	cloned := container.Clone()

	assert.Nil(t, container.Bind(func() Nameable {
		return ImplementsNameable{}
	}))

	var nameable Nameable

	assert.Nil(t, container.Get(&nameable))
	assert.Equal(t, nameable.Name(), "ImplementsNameable")

	assert.Nil(t, cloned.Bind(func() Nameable {
		return SecondTestStruct{}
	}))

	assert.Nil(t, container.Get(&nameable))
	assert.Equal(t, nameable.Name(), "ImplementsNameable")

	assert.Nil(t, cloned.Get(&nameable))
	assert.Equal(t, nameable.Name(), "floof")
}
